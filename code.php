<?php

class Person {
	public $firstName;
	public $middleName;
	public $lastName;

	function __construct($firstName, $middleName){
		$this->first = $firstName;
		$this->middle = $middleName;
	}

	public function printName() {
		return "Your full name is $this->first $this->middle";
	}
}

$person = new Person('Senku', 'Ishigami');

class Developer extends Person {

	function __construct($firstName, $middleName, $lastName){
		$this->first = $firstName;
		$this->middle = $middleName;
		$this->lastname = $lastName;
	}
	
	public function printName() {
		return "Your name is $this->first $this->middle $this->lastname and you are a developer";
	}
}

$developer = new Developer('John', 'Finch', 'Smith');

class Engineer extends Person {

	function __construct($firstName, $middleName, $lastName){
		$this->first = $firstName;
		$this->middle = $middleName;
		$this->lastname = $lastName;
	}
	
	public function printName() {
		return "You are an engineer named $this->first $this->middle $this->lastname";
	}
}

$engineer = new Engineer('Harold', 'Myers', 'Reese');