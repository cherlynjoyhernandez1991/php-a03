<?php require_once 'code.php'; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>OOP Activity</title>
</head>
<body>
	<h2>Person</h2>
	<p><?= $person->printName(); ?></p>

	<h2>Developer</h2>
	<p><?= $developer->printName(); ?></p>

	<h2>Engineer</h2>
	<p><?= $engineer->printName(); ?></p>
</body>
</html>